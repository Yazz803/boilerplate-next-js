import { constant_data } from "@/constants";
import {
  faFacebook,
  faInstagram,
  faLinkedin,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

export default function Footer() {
  return (
    <footer className="my-16 max-w-[1180px] mx-auto px-4">
      <div className="grid grid-cols-12 gap-y-5">
        <div className="lg:col-span-3 col-span-12">
          <p className="text-grey-1 text-2xl">
            {constant_data.appName} Company
          </p>
          <p className="text-grey-2 my-4">
            We have been in business for almost a decade. Helping you is our top
            priority.
          </p>
          <p className="text-grey-1 ">
            © {constant_data.appName} - All Rights Reserved{" "}
            {new Date().getFullYear()}
          </p>
        </div>
        <div className="lg:col-span-6 col-span-12">
          <div className="flex gap-16">
            <div>
              <p className="text-dark-blue font-semibold uppercase mb-6">
                Company
              </p>
              <div className="flex flex-col gap-3">
                <p className="font-medium text-grey-1">Team</p>
                <p className="font-medium text-grey-1">History</p>
                <p className="font-medium text-grey-1">Contact Us</p>
                <p className="font-medium text-grey-1">Locations</p>
              </div>
            </div>
            <div>
              <p className="text-dark-blue font-semibold uppercase mb-6">
                Resources
              </p>
              <div className="flex flex-col gap-3">
                <p className="font-medium text-grey-1">Resource</p>
                <p className="font-medium text-grey-1">Resource name</p>
                <p className="font-medium text-grey-1">Another resource</p>
                <p className="font-medium text-grey-1">Final resource</p>
              </div>
            </div>
            <div>
              <p className="text-dark-blue font-semibold uppercase mb-6">
                Legal
              </p>
              <div className="flex flex-col gap-3">
                <p className="font-medium text-grey-1">Privacy policy</p>
                <p className="font-medium text-grey-1">Terms of use</p>
              </div>
            </div>
          </div>
        </div>
        <div className="lg:col-span-3 col-span-12 flex justify-end">
          <div className="flex gap-4">
            <div className="w-[45px] h-[45px] flex items-center justify-center bg-grey-0 rounded-full p-2 cursor-pointer">
              <FontAwesomeIcon
                icon={faTwitter}
                className="w-[25px] text-sm text-dark-blue"
              />
            </div>
            <div className="w-[45px] h-[45px] flex items-center justify-center bg-grey-0 rounded-full p-2 cursor-pointer">
              <FontAwesomeIcon
                icon={faFacebook}
                className="w-[25px] text-sm text-dark-blue"
              />
            </div>
            <div className="w-[45px] h-[45px] flex items-center justify-center bg-grey-0 rounded-full p-2 cursor-pointer">
              <FontAwesomeIcon
                icon={faInstagram}
                className="w-[25px] text-sm text-dark-blue"
              />
            </div>
            <div className="w-[45px] h-[45px] flex items-center justify-center bg-grey-0 rounded-full p-2 cursor-pointer">
              <FontAwesomeIcon
                icon={faLinkedin}
                className="w-[25px] text-sm text-dark-blue"
              />
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
