"use client";
import { constant_data } from "@/constants";
import { routeName } from "@/routes";
import { faBars, faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";

export default function Navbar() {
  const [isShow, setIsShow] = useState(false);
  const [scrolled, setScrolled] = useState(false);

  const dataNavbar = [
    {
      name: "About",
      link: `/#${""}`,
    },
    {
      name: "Services",
      link: `/#${""}`,
    },
    {
      name: "Our Expertise",
      link: `/#${""}`,
    },
    {
      name: "Our Teams",
      link: `/#${""}`,
    },
    {
      name: "Contact",
      link: `/#${""}`,
    },
  ];

  useEffect(() => {
    const handleScroll = () => {
      const isScrolled = window.scrollY > 100;
      setScrolled(isScrolled);
    };

    window.addEventListener("scroll", handleScroll);

    // Remove the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <React.Fragment>
      {/* DESKTOP NAVBAR */}
      <nav
        className={`z-[50] fixed right-0 left-0 transition-all ${!scrolled ? "mt-14" : "bg-white shadow-lg text-dark p-5"} lg:block hidden`}
      >
        <div
          className={`${!scrolled ? "max-w-[1150px]" : "max-w-[1200px]"} mx-auto`}
        >
          <div className="flex justify-between">
            <div>
              <h3 className="text-2xl font-medium">{constant_data.appName}</h3>
            </div>
            <div
              className={`flex gap-16 ${!scrolled ? "text-white" : "text-dark"}`}
            >
              {dataNavbar.map((item, i) => (
                <Link key={i} className="text-xl font-medium" href={item.link}>
                  <span>{item.name}</span>
                </Link>
              ))}
            </div>
          </div>
        </div>
      </nav>
      {/* END DESKTOP NAVBAR */}

      {/* MOBILE NAVBAR */}
      <nav className="fixed top-0 bg-white w-full z-[50] lg:hidden">
        <div className="flex justify-between items-center mx-5 my-5">
          <Link href={routeName.home}>
            <h3 className="text-2xl font-medium">{constant_data.appName}</h3>
            {/* <Image
              src="/images/logo.png"
              height={500}
              width={500}
              className="w-[50%]"
              alt="Logo {constant_data.appName}"
            /> */}
          </Link>
          <div className="rounded-full border border-white px-4 py-1">
            <div className={`flex gap-2`} onClick={() => setIsShow(true)}>
              <div className="w-5 h-5">
                <FontAwesomeIcon icon={faBars} />
              </div>
            </div>
          </div>
        </div>
      </nav>
      <section
        className={`fixed z-[9999] bg-[#252a2e] h-full w-screen ${
          isShow ? "translate-y-[0%]" : "translate-y-[-200%]"
        } transition ease-in-out duration-500`}
      >
        <div className="absolute right-5 top-5">
          <div
            className="w-10 h-10 text-white flex justify-center items-center"
            onClick={() => setIsShow(false)}
          >
            <FontAwesomeIcon icon={faCircleXmark} size="lg" />
          </div>
        </div>
        <div className="text-center mt-7">
          <Link href={routeName.home}>
            <center>
              <h3 className="text-2xl font-medium text-white">
                {constant_data.appName}
              </h3>
              {/* <Image
                src="/images/logo-white.png"
                height={500}
                width={500}
                className="w-[40%]"
                alt="Logo {constant_data.appName}"
              /> */}
            </center>
          </Link>
          <div className="mx-5">
            {dataNavbar.map((item, i) => (
              <div key={i} className="mt-4 border-b border-gray-600 py-3">
                <Link
                  href={item.link}
                  onClick={() => setIsShow(false)}
                  className="uppercase font-playfair cursor-pointer text-white"
                >
                  {item.name}
                </Link>
              </div>
            ))}
          </div>
        </div>
      </section>
      {/* END MOBILE NAVBAR */}
    </React.Fragment>
  );
}
