import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'
import { constant_data } from './constants';
import { routeName } from './routes';

const pathUrl = constant_data.baseUrlFE;

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
    const { cookies } = request;

    const loggedIn = cookies.get(constant_data.keyCookieLogin);
    const admin = cookies.get(constant_data.keyCookieAdmin);
    const url = request.url;
  
    // hanya admin yang bisa akses dashboard-admin
    if (url.includes("/dashboard-admin")) {
      if(!admin) return NextResponse.redirect(`${pathUrl}${routeName.login}`);
    }
  
    // ketika user sudah login dan dia buka halaman login
    // maka redirect user ke landing page
    const uriLoggedin = [
      routeName.login,
      routeName.register,
    ];
    for (let i = 0; i < uriLoggedin.length; i++) {
      if (loggedIn && url.includes(uriLoggedin[i])) {
        return NextResponse.redirect(`${pathUrl}/`);
      }
    }
  
    // ketika user belum login dan dia buka halaman yg mengharuskan
    // user untuk login, maka redirect user ke landing page
    const customUri = [
      routeName.dashboard,
    ];
    for (let i = 0; i < customUri.length; i++) {
      if (!loggedIn && url.includes(customUri[i])) {
        return NextResponse.redirect(`${pathUrl}/`);
      }
    }
  
    return NextResponse.next();
}