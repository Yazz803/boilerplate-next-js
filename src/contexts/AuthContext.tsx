"use client";
import React, { createContext, useContext, useState } from "react";
import Cookies from "js-cookie";
import { constant_data } from "@/constants";
import api from "../configs/api";
import { ApiEndpoints } from "@/constants/api.endpoints";
import { routeName } from "@/routes";
import { message } from "antd";

type TAuthContext = {
  isLogin: boolean;
  attemptLogin: (values: TValueLogin) => void;
  loadingLogin: boolean;
  setLoadingLogin: React.Dispatch<React.SetStateAction<boolean>>;
};

type TValueLogin = {
  email: string;
  password: string;
};

export const AuthContext = createContext<TAuthContext | null>(null);

export default function AuthContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [loadingLogin, setLoadingLogin] = useState(false);

  const attemptLogin = async (values: TValueLogin) => {
    setLoadingLogin(true);
    const res = await api.postRequest(ApiEndpoints.login, {
      email: values.email,
      password: values.password,
    });
    if (res.success) {
      Cookies.remove(constant_data.keyCookieLogin);
      Cookies.remove(constant_data.keyCookieAdmin);

      // Cookie User Loggedin
      Cookies.set(constant_data.keyCookieLogin, constant_data.keyCookieLogin);
      Cookies.set(constant_data.tokenName, res.token);

      localStorage.setItem(constant_data.tokenName, res.token);
      window.location.href = routeName.home;
      message.success("Berhasil Login");
    }
    setLoadingLogin(false);
  };

  function checkIsLogin() {
    if (
      Cookies.get(constant_data.keyCookieLogin) &&
      Cookies.get(constant_data.tokenName) !== undefined
    ) {
      return true;
    }
    return false;
  }
  return (
    <AuthContext.Provider
      value={{
        attemptLogin,
        loadingLogin,
        setLoadingLogin,
        isLogin: checkIsLogin(),
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export function useAuthContext() {
  const context = useContext<TAuthContext | null>(AuthContext);
  if (!context)
    throw new Error("useAuthContext must be used within AuthContextProvider");
  return context;
}
