"use client";
import React, { createContext, useContext } from "react";
import { useMediaQuery } from "react-responsive";

type TResponsiveContext = {
  isMobile: boolean;
  isTablet: boolean;
  isLaptop: boolean;
  isDesktop: boolean;
  isDesktopL: boolean;
  isDesktopXL: boolean;
};

export const ResponsiveContext = createContext<TResponsiveContext | null>(null);

export default function ResponsiveContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const isMobile = useMediaQuery({ query: "(max-width: 640px)" });
  const isTablet = useMediaQuery({ query: "(max-width: 768px)" });
  const isLaptop = useMediaQuery({ query: "(max-width: 1024px)" });
  const isDesktop = useMediaQuery({ query: "(max-width: 1280px)" });
  const isDesktopL = useMediaQuery({ query: "(max-width: 1536px)" });
  const isDesktopXL = useMediaQuery({ query: "(max-width: 1920px)" });

  return (
    <ResponsiveContext.Provider
      value={{
        isMobile,
        isTablet,
        isLaptop,
        isDesktop,
        isDesktopL,
        isDesktopXL,
      }}
    >
      {children}
    </ResponsiveContext.Provider>
  );
}

export function useResponsiveContext() {
  const context = useContext<TResponsiveContext | null>(ResponsiveContext);
  if (!context)
    throw new Error(
      "useResponsiveContext must be used within ResponsiveContextProvider",
    );
  return context;
}
