import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import NextTopLoader from 'nextjs-toploader';
import { ConfigProvider } from 'antd';
import locale from 'antd/locale/id_ID';
import AuthContextProvider from '@/contexts/AuthContext';
import ResponsiveContextProvider from '@/contexts/ResponsiveContext';
import { AntdRegistry } from '@ant-design/nextjs-registry';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Your Application',
  description: 'Description for Your Application',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <NextTopLoader
          color="#FFD92C"
          initialPosition={0.08}
          crawlSpeed={200}
          height={5}
          crawl={true}
          easing="ease"
          speed={200}
          shadow="0 0 10px #FFD92C,0 0 5px #FFD92C"
          template={`<div class="bar" role="bar"><div class="peg"></div></div> 
          <div class="spinner" role="spinner"><div class="spinner-icon"></div></div>`}
          zIndex={1600}
          showAtBottom={false}
        />
        <ConfigProvider
          theme={{
            token: {
              // Seed Token
              colorPrimary: '#0D47C0',
              borderRadius: 2,
            },
          }}
          locale={locale}
        >
          <AntdRegistry>
            <AuthContextProvider>
              <ResponsiveContextProvider>{children}</ResponsiveContextProvider>
            </AuthContextProvider>
          </AntdRegistry>
        </ConfigProvider>
      </body>
    </html>
  );
}
