export const constant_data = Object.freeze({
    baseUrlFE: "https://dashboard-skyminer.vercel.app",
    baseUrlBE: process.env.BASE_URL_BE ?? "",

    keyCookieLogin: process.env.KEY_COOKIE_LOGIN ?? "",
    keyCookieAdmin: process.env.KEY_COOKIE_ADMIN ?? "",

    tokenName: process.env.TOKEN_NAME ?? "" ,

    appName: "Dashboard Skyminer",
}) 