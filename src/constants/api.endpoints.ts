export const ApiEndpoints = Object.freeze({
    login: "/auth/login",
    register: "/auth/register",
})