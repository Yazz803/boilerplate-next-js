export const routeName = Object.freeze({
    login: "/login",
    register: "/register",
    dashboard: "/dashboard",
    dashboardAdmin: "/dashboard-admin",
    notFound: "/404",
    error: "/500",
    home: "/"
})